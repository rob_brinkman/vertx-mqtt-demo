package com.jdriven;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class VertxMqttDemoLauncher {

    private final static Logger log = LoggerFactory.getLogger(VertxMqttDemoLauncher.class);

    public static void main(String... args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle("com.jdriven.VertxMqttDemoVerticle", result -> {
            if (result.succeeded()) {
                log.info("Deployed verticle");
            } else {
                log.error("Failed to deploy verticle, reason: {0}", result.cause().getMessage(), result.cause());
            }
        });
    }

}
