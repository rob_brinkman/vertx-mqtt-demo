package com.jdriven;

import io.netty.handler.codec.mqtt.MqttConnectReturnCode;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.mqtt.MqttServer;
import io.vertx.mqtt.MqttServerOptions;
import io.vertx.mqtt.examples.verticle.MqttServerVerticle;

public class VertxMqttDemoVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MqttServerVerticle.class);

    private MqttServer server;

    @Override
    public void start() throws Exception {

        MqttServerOptions options = new MqttServerOptions();
        options.setHost("127.0.0.1").setPort(1883);

        server = MqttServer.create(this.vertx, options);

        server.endpointHandler(endpoint -> {
            log.debug("Client connected: {0}", endpoint.clientIdentifier());
            endpoint.publishHandler(message -> {
                log.info("Received message at {0}, payload: {1}, qosLevel: {2}", message.topicName(), message.payload(), message.qosLevel());
            });
            endpoint.writeConnack(MqttConnectReturnCode.CONNECTION_ACCEPTED, false);
        });

        this.server.listen(result -> {
            if (result.succeeded()) {
                log.info("MQTT server started on port: {0}", result.result().actualPort());
            } else {
                log.error("MQTT server failed to start: {0}", result.cause().getMessage(), result.cause());
            }
        });
    }
}